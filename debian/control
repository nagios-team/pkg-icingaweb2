Source: icingaweb2
Maintainer: Debian Nagios Maintainer Group <pkg-nagios-devel@lists.alioth.debian.org>
Uploaders: Markus Frosch <lazyfrosch@debian.org>
Section: admin
Priority: optional
Build-Depends: bash-completion,
               debhelper-compat (= 13),
               dh-sequence-bash-completion,
               php-cli
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/nagios-team/icingaweb2
Vcs-Git: https://salsa.debian.org/nagios-team/icingaweb2.git
Homepage: https://icinga.com
Rules-Requires-Root: no

Package: icingaweb2
Architecture: all
Depends: fonts-dejavu-core,
         fonts-dejavu-extra,
         icingaweb2-common (= ${source:Version}),
         php-xml,
         ${misc:Depends}
Recommends: apache2 | httpd,
            php,
            php-cli,
            php-curl,
            php-dom,
            php-gd,
            php-mbstring,
            php-ldap,
            php-json,
            php-intl,
            php-imagick,
            icingacli,
            icingaweb2-module-monitoring,
            icingaweb2-module-doc,
            nagios-images
Description: simple and responsive web interface for Icinga
 Icinga Web 2 is a very modular, fast and simple web interface for your Icinga
 monitoring environment.
 .
 The software will give you a web frontend for your monitoring solution, and
 can run additional modules, extending monitoring data, or even supplying
 something new to the webinterface.
 .
 This package installs the web interface with all needed dependencies.

Package: icingaweb2-common
Architecture: all
Depends: adduser,
         icinga-php-library (>= 0.13.0),
         icinga-php-thirdparty (>= 0.12.0),
         php-cli,
         php-icinga (= ${source:Version}),
         ${misc:Depends}
Conflicts: icingaweb2-module-setup
Replaces: icingaweb2-module-setup
Description: simple and responsive web interface for Icinga - common files
 Icinga Web 2 is a very modular, fast and simple web interface for your Icinga
 monitoring environment.
 .
 The software will give you a web frontend for your monitoring solution, and
 can run additional modules, extending monitoring data, or even supplying
 something new to the webinterface.
 .
 This package contains common files for the web interface, and the CLI tool.

Package: icingaweb2-module-monitoring
Architecture: all
Depends: icingaweb2 (= ${source:Version}),
         ${misc:Depends}
Description: simple and responsive web interface for Icinga - monitoring module
 Icinga Web 2 is a very modular, fast and simple web interface for your Icinga
 monitoring environment.
 .
 This module adds the Icinga monitoring frontend to the web interface.

Package: icingaweb2-module-doc
Architecture: all
Section: doc
Depends: icingaweb2 (= ${source:Version}),
         ${misc:Depends}
Description: simple and responsive web interface for Icinga - documentation module
 Icinga Web 2 is a very modular, fast and simple web interface for your Icinga
 monitoring environment.
 .
 This module adds the documentation viewer of Icinga Web 2, which can also
 display the documentation of other modules.

Package: php-icinga
Architecture: all
Section: php
Depends: php,
         ${misc:Depends}
Recommends: php-mysql | php-pgsql,
            php-json
Description: PHP library to communicate with and use Icinga
 Icinga Web 2 is a very modular, fast and simple web interface for your Icinga
 monitoring environment.
 .
 Icinga is a PHP library providing a PHP API to interact with Icinga's data
 and other core parts of Icinga web 2 that may be used by modules.
 .
 This library is mainly used by Icinga Web 2 to get its data.

Package: icingacli
Architecture: all
Depends: adduser,
         icingaweb2-common (= ${source:Version}),
         ${misc:Depends}
Recommends: php-cli
Suggests: icingaweb2-module-monitoring
Pre-Depends: ${misc:Pre-Depends}
Description: simple CLI tool for Icingaweb2 and its modules
 Icinga Web 2 is a very modular, fast and simple web interface for your Icinga
 monitoring environment.
 .
 `icingacli` is a command line utility, allowing the admin to configure Icinga
 Web 2 and provides the possibility to query Icinga data from your shell.
 All modules of Icingaweb2 can add additional commands to the icingacli.
